package junitmockito;

public class App {

	public static void main(String[] args) {
		Sales sales = new Sales();//assuming Sales is ready

		Tax tax = new Tax();
		double txAmount = tax.calcTaxAmt(2021, sales); 
		
		System.out.printf("Tax for the year 2021 is %.2f",txAmount);
	}
}
