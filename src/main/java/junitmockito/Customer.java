package junitmockito;

import java.time.*;

public class Customer {
	private LocalDate dob;
	
	public Customer(LocalDate dob) {
		this.dob = dob;
	}
	
	public boolean isSenior() {
		boolean result = false;
		
		LocalDate today = LocalDate.now(); 
		Period difPeriod = Period.between(dob, today);
		int age = difPeriod.getYears();
		
		if(age>=60) result = true;
		
		return result;
	}
}
