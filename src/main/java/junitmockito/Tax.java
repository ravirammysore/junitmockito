package junitmockito;

public class Tax {
	
	public double calcTaxAmt(int year, Sales sales) {			
		
		double salesAmt = sales.calcSalesAmt(year);
		double invAmt = sales.calcInvAmr(year);	
		
		double taxAmt = (salesAmt-invAmt) * (0.3);	
		
		return taxAmt;
	}			
}
  