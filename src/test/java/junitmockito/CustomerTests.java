package junitmockito;

import java.time.LocalDate;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class CustomerTests {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@Test 
	public void test_IsSenior_MustReturnTrue() {
		LocalDate dob = LocalDate.parse("1960-01-01");
		Customer c = new Customer(dob);
		Assert.assertTrue(c.isSenior());	
	}
	
	@Test
	public void test_IsSenior_MustReturnFalse() {
		LocalDate dob = LocalDate.parse("1980-01-01");
		Customer c = new Customer(dob);
		Assert.assertFalse(c.isSenior());	
	}
}
