package junitmockito;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;

import junitmockito.Sales;
import junitmockito.Tax;

public class TaxTest {
	
	@Test
	public void test_CalcTaxAmt() {		
		
		Sales salesMock = mock(Sales.class);		
		when(salesMock.calcSalesAmt(2021)).thenReturn((double)2000000);
		when(salesMock.calcInvAmr(2021)).thenReturn((double)500000);
			
		Tax tax = new Tax();			
		double actualAmt = tax.calcTaxAmt(2021,salesMock);		
		Assert.assertEquals(450000, actualAmt,0);
	}
}
